extends TextureRect

#const tile_font = preload("res://addons/ShadeStack/tile-font.tres")
const Tile = preload("res://tile.gd")

const LOWER_BOUND = -4
const UPPER_BOUND = 4
const CENTER = 512 # board texture is 1024x1024
const HEX_RADIUS = 65
const CELL_WIDTH = sqrt(3) * HEX_RADIUS # width of a hexagon across flats
const DROP_RADIUS = 55 # radius from center of cell that drag and drop works

# Keys are xy coords of cells in the hex grid.
# Values are the pixel positions of the center of the cell.
var cell_coords = {}  

func _ready():
	for i in range(LOWER_BOUND, UPPER_BOUND + 1):
		for j in range(LOWER_BOUND, UPPER_BOUND + 1):
			if is_cell_coord_on_board(i, j):
				var pixel_coords = get_pixel_coords(Vector2(i, j))
				cell_coords[Vector2(i, j)] = pixel_coords


# Uncomment this function to see red circles where drag and drop works
func _draw():
	for key in cell_coords.keys():
		draw_polyline(get_hex_points(cell_coords[key]),
		              Color(1, 1, 1, 0.75),
		              3.0,
		              true)
#		draw_circle(cell_coords[key], DROP_RADIUS, Color(1, 0, 0))
#		draw_cell_coord_label(key.x, key.y, get_pixel_coords(key))


func get_hex_points(center): # return the points of a regular hexagon
	var points = []
	var radians = PI / 6 # to get pointy-topped hexagons
	for i in range(0, 7): # last point is same as first to close the lines
		points.append(Vector2(center.x + HEX_RADIUS * cos(radians),
		                      center.y + HEX_RADIUS * sin(radians)))
		radians += PI / 3
	return points


func clear():
	for child in get_children():
		remove_child(child)
		child.queue_free()
	update()


# Check to make sure a potential point for a cell is within the board bounds.
# This keeps the overall shape of the board to a hexagon, with 5 hexagonal cells
# per side.
func is_cell_coord_on_board(x, y):
	if abs(x + y) > 4:
		return false
	else:
		return true


func get_pixel_coords(point):
	var x = CENTER
	var y = CENTER
	
	y += cos(PI/6) * CELL_WIDTH * point.y
	x += CELL_WIDTH * point.x
	
	 # offset X according to 60 degree angle of Y axis
	x += CELL_WIDTH / 2 * point.y
	
	return Vector2(x, y)


#func draw_cell_coord_label(x, y, pos):
#	var lbl = Label.new()
#	lbl.text = str(x) + ", " + str(y)
#	lbl.set("custom_fonts/font", tile_font)
#	add_child(lbl)
#	lbl.rect_position = Vector2(pos.x - 25, pos.y - 15)


func can_drop_data(position, data):
	if data is Tile:
		if data.is_placed or not data.hand.has_placed_tile():
			for key in cell_coords.keys():
				if position.distance_to(cell_coords[key]) <= DROP_RADIUS:
					return true
			return false # not within 25px of a grid point
		else:
			return false
	else:
		return false


func drop_data(pos, data):
	data.get_parent().remove_child(data) # remove data from previous parent
	add_child(data)
	
	var nearest_cell = get_nearest_cell(pos)
	if nearest_cell != null:
		data.center_position = cell_coords[nearest_cell]
		data.is_placed = true



func get_nearest_cell(pos):
	for key in cell_coords.keys():
		if pos.distance_to(cell_coords[key]) <= DROP_RADIUS:
			return key
	return null # this will fail loudly and force the calling function to handle the null case


func get_adjacent_tiles(cell_coord):
	var adjacent_cells = [
	  Vector2(cell_coord.x + 1, cell_coord.y),
	  Vector2(cell_coord.x - 1, cell_coord.y),
	  Vector2(cell_coord.x, cell_coord.y + 1),
	  Vector2(cell_coord.x, cell_coord.y - 1),
	  Vector2(cell_coord.x + 1, cell_coord.y - 1),
	  Vector2(cell_coord.x - 1, cell_coord.y + 1) ]
	
	var adjacent_tiles = {}
	
	for tile in get_children():
		var tile_cell_coord = get_nearest_cell(tile.center_position)
		for cell in adjacent_cells:
			if tile_cell_coord == cell:
				if cell == Vector2(cell_coord.x, cell_coord.y - 1):
					adjacent_tiles["nw"] = tile
				elif cell == Vector2(cell_coord.x + 1, cell_coord.y - 1):
					adjacent_tiles["ne"] = tile
				elif cell == Vector2(cell_coord.x + 1, cell_coord.y):
					adjacent_tiles["e"] = tile
				elif cell == Vector2(cell_coord.x, cell_coord.y + 1):
					adjacent_tiles["se"] = tile
				elif cell == Vector2(cell_coord.x - 1, cell_coord.y + 1):
					adjacent_tiles["sw"] = tile
				elif cell == Vector2(cell_coord.x - 1, cell_coord.y):
					adjacent_tiles["w"] = tile
				else:
					print("ERROR: PROGRAM SHOULD NEVER REACH THIS POINT")
				break
	return adjacent_tiles


func compare_tiles(tile, neighbors):
	for key in neighbors.keys():
		if tile.color != neighbors[key].color:
			var defense = 0
			var attack = 0
			match key:
				"nw":
					defense = neighbors[key].powers["se"]
					attack = tile.powers[key]
				"ne":
					defense = neighbors[key].powers["sw"]
					attack = tile.powers[key]
				"e":
					defense = neighbors[key].powers["w"]
					attack = tile.powers[key]
				"se":
					defense = neighbors[key].powers["nw"]
					attack = tile.powers[key]
				"sw":
					defense = neighbors[key].powers["ne"]
					attack = tile.powers[key]
				"w":
					defense = neighbors[key].powers["e"]
					attack = tile.powers[key]
				_:
					print("ERROR: PROGRAM SHOULD NEVER REACH THIS POINT")
			if attack < 4 and attack > defense:
				neighbors[key].toggle()
				get_parent().update_debug_labels(get_parent().get_score())
				yield(get_tree().create_timer(0.3), "timeout")
				
				# cascade
				var neighbor_adj_tiles = get_adjacent_tiles(get_nearest_cell(neighbors[key].center_position))
				if neighbor_adj_tiles.size() > 0:
					for n in neighbor_adj_tiles.keys():
						compare_tiles(neighbors[key], neighbor_adj_tiles)








