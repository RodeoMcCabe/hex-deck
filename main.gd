extends Panel

const Tile = preload("res://tile.tscn")

onready var tiles_in_deck_lbl = $Debug/Label2
onready var turn_lbl = $Debug/Label8

onready var player_score_lbl = $Score/Label2
onready var ai_score_lbl = $Score/Label4
onready var win_lbl = $WinLbl

onready var board = $Board
onready var player_hand = $Player
onready var ai_hand = $AI
onready var end_turn_button = $EndTurnBtn

var deck = []
var active_tile = null

func _ready():
	randomize() # seed random functions
	new_game()


func build_deck():
	for i in range(0, 2):
		instance_tile_set(1, 2)
		instance_tile_set(1, 3)
		instance_tile_set(2, 3)
		instance_tile_set(2, 4)
	for i in range(0, 3):
		instance_tile_set(2, 1)
		instance_tile_set(3, 1)
	
	instance_tile_set(3, 2)
	instance_tile_set(4, 2)


# Creates a set of tiles with the power of each side set. If base and bonus are
# the same, the power of all sides will be the same. If bonus is different, one
# pair of opposing sides will have power equal to bonus. A set of tiles is made
# up of three tiles, one for each pair of opposing sides.
func instance_tile_set(base, bonus): 
	var e_w = Tile.instance() # opposing sides are east and west
	var ne_sw = Tile.instance()
	var nw_se = Tile.instance()

	e_w.connect("placed", self, "_on_Tile_placed")
	ne_sw.connect("placed", self, "_on_Tile_placed")
	nw_se.connect("placed", self, "_on_Tile_placed")
	
	e_w.connect("unplaced", self, "_on_Tile_unplaced")
	ne_sw.connect("unplaced", self, "_on_Tile_unplaced")
	nw_se.connect("unplaced", self, "_on_Tile_unplaced")
	
	for side in e_w.powers.keys(): # keys will work for any Tile instance
		e_w.powers[side] = base
		ne_sw.powers[side] = base
		nw_se.powers[side] = base
	
	e_w.powers["e"] = bonus
	e_w.powers["w"] = bonus
	ne_sw.powers["ne"] = bonus
	ne_sw.powers["sw"] = bonus
	nw_se.powers["nw"] = bonus
	nw_se.powers["se"] = bonus
	
	deck.append(e_w)
	deck.append(ne_sw)
	deck.append(nw_se)


# Shuffles the deck using the Fisher-Yates algorithm.
func shuffle_deck():
	var elements_to_shuffle = deck.size() -1
	var index
	var temp
	
	while(elements_to_shuffle > 0):
		index = rand_range(0, elements_to_shuffle)
		temp = deck[elements_to_shuffle]
		deck[elements_to_shuffle] = deck[index]
		deck[index] = temp
		
		elements_to_shuffle -= 1

func new_game():
	deck.clear()
	board.clear()
	player_hand.clear()
	ai_hand.clear()
	
	build_deck()
	shuffle_deck()
	
	for i in range(0, 10):
		if i % 2 == 0:
			player_hand.draw_tile("blue")
		else:
			ai_hand.draw_tile("red")
	
	ai_hand.hide()
	player_hand.show()
	
	update_debug_labels(get_score())
	turn_lbl.text = "blue"


func get_score():
	var blue = 0
	var red = 0
	for tile in board.get_children():
		if tile.color == "blue":
			blue += 1
		else:
			red += 1
	return { "blue" : blue, "red" : red }


func update_debug_labels(score_dict):
	player_score_lbl.text = str(score_dict["blue"])
	ai_score_lbl.text = str(score_dict["red"])
	tiles_in_deck_lbl.text = str(deck.size())


func _on_Tile_placed(tile):
	end_turn_button.disabled = false
	active_tile = tile


func _on_Tile_unplaced():
	end_turn_button.disabled = true
	active_tile = null


func _on_EndTurnBtn_pressed():
	if not active_tile.hand.has_placed_tile():
		print("ERROR: place a tile before you end your turn, you nitwit!")
	else:
		var cell = board.get_nearest_cell(active_tile.center_position)
		var neighbors = board.get_adjacent_tiles(cell)
		board.compare_tiles(active_tile, neighbors)
		active_tile.hand.hand.erase(active_tile)
		active_tile.is_placed = false
		active_tile = null
	
	if turn_lbl.text == "blue":
		player_hand.hide()
		player_hand.draw_tile("blue")
		ai_hand.show()
		turn_lbl.text = "red"
	else:
		ai_hand.hide()
		ai_hand.draw_tile("red")
		player_hand.show()
		turn_lbl.text = "blue"
		
	end_turn_button.disabled = true
	
	var score = get_score()
	update_debug_labels(score)
	if board.get_child_count() >= 48: # all tiles placed
		if score["blue"] > score["red"]:
			win_lbl.text = "blue wins!"
		elif score["red"] > score["blue"]:
			win_lbl.text = "red wins!"
		else:
			win_lbl.text = "it's a tie!"
		win_lbl.show()


func _on_RestartBtn_pressed():
	win_lbl.hide()
	
	new_game()
