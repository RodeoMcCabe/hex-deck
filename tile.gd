extends TextureRect

enum Power {
	LOW = 1,
	MID,
	HIGH,
	SHIELD }

onready var sides = {
	"nw" : $NorthWest,
	"ne" : $NorthEast,
	"e" : $East,
	"se" : $SouthEast,
	"sw" : $SouthWest,
	"w" : $West }

var powers = {
	"nw" : LOW,
	"ne" : LOW,
	"e" : LOW,
	"se" : LOW,
	"sw" : LOW,
	"w" : LOW }

const TILE_BLUE = preload("res://tile-blue.png")
const TILE_RED = preload("res://tile-red.png")
const NUMERAL_1 = preload("res://1.png")
const NUMERAL_2 = preload("res://2.png")
const NUMERAL_3 = preload("res://3.png")
const SHIELD_IMG = preload("res://shield.png")

onready var hand = get_node("../..") # the hand this tile belongs to
onready var highlight = $Highlight
var center_position setget set_center_position, get_center_position
var color = "blue" setget set_color, get_color
var is_placed = false setget set_placed, get_placed

signal placed
signal unplaced # taken back into player's hand

func _ready():
	for key in powers.keys():
		if powers[key] == 1:
			sides[key].texture = NUMERAL_1
		elif powers[key] == 2:
			sides[key].texture = NUMERAL_2
		elif powers[key] == 3:
			sides[key].texture = NUMERAL_3
		else:
			sides[key].texture = SHIELD_IMG


func get_drag_data(pos):
	if (is_placed and hand.has_placed_tile())\
	  or (not is_placed\
	    and not hand.has_placed_tile()
		  and not get_parent().name == "Board"):
		var preview = self.duplicate(16)# make a new Tile instance
		preview.rect_scale *= 0.75
		set_drag_preview(preview)
		return self


func set_center_position(pos):
	rect_position = Vector2(pos.x - 64, pos.y - 64)

func get_center_position():
	return Vector2(rect_position.x + 64, rect_position.y + 64)


func set_color(new_color):
	if new_color == "blue":
		texture = TILE_BLUE
		color = "blue"
	elif new_color == "red":
		texture = TILE_RED
		color = "red"
	else:
		print("ERROR: invalid color passed to Tile.set_color()")
	update()

func get_color():
	return color


func set_placed(placed):
	if placed:
		highlight.show()
		is_placed = true
		emit_signal("placed", self)
	else:
		highlight.hide()
		is_placed = false
		emit_signal("unplaced")

func get_placed():
	return is_placed


func toggle(): # switches tile between being owned by the player or the enemy 
	if color == "blue":
		texture = TILE_RED
		color = "red"
	else:
		texture = TILE_BLUE
		color = "blue"
	update()

