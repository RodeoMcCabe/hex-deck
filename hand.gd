extends PanelContainer

onready var Tile = preload("res://tile.gd")
onready var deck = get_parent().deck
onready var hbox = $HBox

var hand = []


func draw_tile(color):
	if hand.size() < 5 and deck.size() > 0:
		var tile = deck.pop_front()
		tile.color = color
		hand.append(tile)
		hbox.add_child(tile)
		update()


func has_placed_tile():
	for tile in hand:
		if tile.is_placed == true:
			return true
	return false


func clear():
	if hand.size() > 0:
		for child in hbox.get_children():
			hbox.remove_child(child)
	hand.clear()


func can_drop_data(position, data):
	if data is Tile:
		return true
	else:
		return false


func drop_data(position, data):
	data.get_parent().remove_child(data)
	hbox.add_child(data)
	data.is_placed = false